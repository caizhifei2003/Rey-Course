const app = getApp()
const MyDate = require('../../utils/MyDate.js')

Page({
    data: {
        days: []
    },
    onLoad: function () {
        var weekDays = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        var days = new MyDate().thisWeek(1).map(item => {
            return {
                today: item.today,
                date: item.format('dd日'),
                day: weekDays[item.date.getDay()],
                numDay: item.date.getDay()
            };
        });
        this.setData({ days: days });
    }
})
