class MyDate {
    get date() { return this._date; }
    get today() {
        var now = new Date();
        return now.getFullYear() === this._date.getFullYear()
            && now.getMonth() === this._date.getMonth()
            && now.getDate() === this._date.getDate();
    }

    constructor(date) {
        this._date = date || new Date();
    }

    firstDayOfWeek(firstDay) {
        if (typeof (firstDay) !== 'number') {
            console.error("first day must be a number");
            return null;
        }

        if (firstDay < 0 || firstDay > 6) {
            console.error('invalid first day');
            return null;
        }

        var day = this._date.getDay() - firstDay;
        if (day < 0) { day += 7; }

        var monday = new Date(this._date.getTime());
        monday.setDate(monday.getDate() - day);
        return monday;
    }

    thisWeek(firstDay) {
        var monday = this.firstDayOfWeek(firstDay);
        var days = [];
        for (var i = 0; i < 7; ++i) {
            var date = new Date(monday.getTime());
            date.setDate(date.getDate() + i);
            days.push(new MyDate(date));
        }
        return days;
    }

    format(text) {
        return text
            .replace(/yyyy/, this._date.getFullYear())
            .replace(/MM/, this._date.getMonth() + 1)
            .replace(/dd/, this._date.getDate())
            .replace(/hh/, this._date.getHours())
            .replace(/mm/, this._date.getMinutes())
            .replace(/ss/, this._date.getSeconds());
    }
}

module.exports = MyDate;